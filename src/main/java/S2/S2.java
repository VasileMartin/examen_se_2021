package S2;

import java.time.LocalDateTime;

public class S2 extends Thread {
    private String name;

    public S2(String name) {
        super(name);
        this.name = name;
    }

    public static void main(String[] args) {
        S2 thread1 = new S2("CThread1");
        thread1.start();
        S2 thread2 = new S2("CThread2");
        thread2.start();

    }
    public void run() {
        int i=0;
        while(i<7) {
            System.out.println(name+"-"+ LocalDateTime.now());
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            i++;
        }
    }
}
