package S1;

import java.util.LinkedList;
import java.util.List;

public class B {
    private long t;
    private E e;

    public B(long t, E e) {
        this.t = t;
        this.e = e;
    }

    void x(){
        System.out.println("x function");
    }
}

interface C {
}

class D {
    public D(B d) {
    }
}



class E {
    private List<H> hList= new LinkedList<>();
    void metG(int i){
        System.out.println(i);
    }
}

class F {
    private List<E> eList= new LinkedList<>();

    void metA() {
        System.out.println("Composed class");
    }
}

class G {
}

class H {
}
